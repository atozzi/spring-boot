package hello.api.rest;

import com.amazonaws.services.cloudsearchdomain.AmazonCloudSearchDomainClient;
import com.amazonaws.services.cloudsearchdomain.model.Hit;
import com.amazonaws.services.cloudsearchdomain.model.QueryParser;
import com.amazonaws.services.cloudsearchdomain.model.SearchRequest;
import com.amazonaws.services.cloudsearchdomain.model.SearchResult;
import hello.domain.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by andre on 26/07/2016.
 */

@RestController
@RequestMapping(value = "/wayfinder/v1")
public class DemoController {
    RestTemplate restTemplate = new RestTemplate();

    public static final Map<String, Locale> localeMap = Arrays.asList(
            Locale.ITALY, Locale.GERMANY, Locale.UK)
            .stream()
            .collect(Collectors.toMap(l -> l.getLanguage(), Function.identity()));
    private static final String ORDER_BY = "ranking desc";
    private static final String SEARCH_FIELDS = "{fields:['name', 'alternatenames']}";
    private static final Map<String, Integer> fcodeSortMap = Collections.unmodifiableMap(Stream.of(
            new AbstractMap.SimpleEntry<>("PPLC", 9),
            new AbstractMap.SimpleEntry<>("PPLA", 8),
            new AbstractMap.SimpleEntry<>("PPLA2", 7),
            new AbstractMap.SimpleEntry<>("PPLA3", 6),
            new AbstractMap.SimpleEntry<>("PPLA4", 5),
            new AbstractMap.SimpleEntry<>("PPL", 4),
            new AbstractMap.SimpleEntry<>("AIRP", 3),
            new AbstractMap.SimpleEntry<>("RSTN", 2),
            new AbstractMap.SimpleEntry<>("PRT", 1))
            .collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue())));
    private final BiFunction<Map<String, List<String>>, String, String> searchResponseResolver = new BiFunction<Map<String, List<String>>, String, String>() {
        @Override
        public String apply(Map<String, List<String>> stringListMap, String s) {
            return stringListMap.getOrDefault(s, Collections.emptyList()).isEmpty()
                    ? ""
                    : stringListMap.get(s).stream().findFirst().get();
        }
    };
    @Autowired
    private AmazonCloudSearchDomainClient amazonCloudSearchDomainClient;

    @RequestMapping(value = "/locations",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    //@Timed
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    List<Location> searchResult(
            @RequestParam(value = "q", required = true) String q,
            @RequestParam(value = "pretty", required = false) boolean pretty,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "admin1name", required = false) String admin1Name,
            @RequestParam(value = "country", required = false) String countryCode,
            HttpServletRequest httpServletRequest) {

        String acceptLanguage = Optional.ofNullable(httpServletRequest.getHeader("Accept-Language")).orElse("en");
        final String q1 = (q.endsWith("*") ? q.substring(0, q.length() - 1) : q).replace("\'", "\\'");
        String keySearch = "";
        SearchRequest request = new SearchRequest();

        if (admin1Name != null || countryCode != null) {
            admin1Name = admin1Name != null ? admin1Name.replace("\'", "\\'") : admin1Name;
            request.setQueryParser(QueryParser.Structured);
            String s = "(and %s %s %s)";
            String paramOne = "(or name:'%s' alternatenames:'%s')";
            String paramTwo = "(or admin1_name:'%s' admin1_name_it:'%s' admin1_name_en:'%s' admin1_name_de:'%s')";
            String paramThree = "country:'%s'";

            keySearch = (admin1Name != null || countryCode != null) ?
                    String.format(s, String.format(paramOne, q, q),
                            admin1Name != null ? String.format(paramTwo, admin1Name, admin1Name, admin1Name, admin1Name) : "",
                            countryCode != null ? String.format(paramThree, countryCode) : "") :
                    String.format(paramOne, q, q);
        } else {
            request.setQueryParser(QueryParser.Simple);
            request.setQueryOptions(SEARCH_FIELDS);
            String single = "%s";
            String multiple = "%s | %s*";
            String fuzzy = "%s | %s* | %s~1";

            keySearch = q.trim().length() <= 3 ?
                    q.endsWith(" ") ? String.format(single, q) :
                            String.format(multiple, q, q) :
                    String.format(fuzzy, q, q, q.toLowerCase().trim());
        }
        size = (size == null || size > 200) ? 20 : size;
        Locale locale = localeMap.getOrDefault(acceptLanguage, Locale.UK);

        request.setQuery(keySearch);
        request.setSort(ORDER_BY);
        request.setSize((long) size);
        SearchResult result = amazonCloudSearchDomainClient.search(request);

        List<Location> hits = result.getHits()
                .getHit()
                .stream()
                .map(h -> asLocation(h, locale))
                .collect(Collectors.toList());

        List<Location> locations = hits.stream()
                .filter(l -> l.getName().toLowerCase().equals(q1.toLowerCase()))
                .collect(Collectors.toList());

        locations.addAll(hits.stream()
                .filter(l -> l.getName().toLowerCase().contains(q1.toLowerCase()) && !l.getName().toLowerCase().equals(q1.toLowerCase()))
                .sorted((new Comparator<Location>() {
                    @Override
                    public int compare(Location o1, Location o2) {
                        return fcodeSortMap
                                .getOrDefault(o1.getfCode(), 100)
                                .compareTo(fcodeSortMap.getOrDefault(o2.getfCode(), 100));
                    }
                }).thenComparingInt(Location::getRanking).reversed())
                .collect(Collectors.toList()));

        locations.addAll(hits.stream()
                .filter(l -> !l.getName().toLowerCase().contains(q1.toLowerCase()))
                .sorted((new Comparator<Location>() {
                    @Override
                    public int compare(Location o1, Location o2) {
                        return fcodeSortMap
                                .getOrDefault(o1.getfCode(), 100)
                                .compareTo(fcodeSortMap.getOrDefault(o2.getfCode(), 100));
                    }
                }).thenComparingInt(Location::getRanking).reversed())
                .collect(Collectors.toList()));
        return locations;
    }

    public Location asLocation(Hit hit, Locale language) {
        Map<String, List<String>> fields = hit.getFields();
        Location location = new Location();

        location.setContinent(searchResponseResolver.apply(fields, "continent"));
        location.setfClass(searchResponseResolver.apply(fields, "fclass"));
        location.setCountry(searchResponseResolver.apply(fields, "country"));
        location.setTimezone(searchResponseResolver.apply(fields, "timezone"));
        location.setName(
                searchResponseResolver.apply(fields, "alternatename_" + language.getLanguage()).isEmpty()
                        ? searchResponseResolver.apply(fields, "name")
                        : searchResponseResolver.apply(fields, "alternatename_" + language.getLanguage()));
        location.setAdmin1(searchResponseResolver.apply(fields, "admin1"));
        location.setAdmin1Name(searchResponseResolver.apply(fields, "admin1_name_" + language.getLanguage()).isEmpty()
                ? searchResponseResolver.apply(fields, "admin1_name")
                : searchResponseResolver.apply(fields, "admin1_name_" + language.getLanguage()));
        location.setAdmin2(searchResponseResolver.apply(fields, "admin2"));
        location.setAdmin3(searchResponseResolver.apply(fields, "admin3"));
        location.setAdmin4(searchResponseResolver.apply(fields, "admin4"));
        location.setAsciiName(searchResponseResolver.apply(fields, "asciiname"));
        location.setfCode(searchResponseResolver.apply(fields, "fcode"));
        location.setRanking(Integer.parseInt(searchResponseResolver.apply(fields, "ranking")));
        location.setLatitude(Double.parseDouble(searchResponseResolver.apply(fields, "location").split(",")[0].trim()));
        location.setLongitude(Double.parseDouble(searchResponseResolver.apply(fields, "location").split(",")[1].trim()));
        return location;
    }

    @RequestMapping(value = "/dnstest",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    String dnsTest() {
        try {
            return restTemplate.getForObject("https://viando.it",
                    String.class);
        } catch (Exception e) {

            return e.getMessage();
        }
    }
}
